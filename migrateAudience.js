const faker = require("faker");

const aixpTracker = require("@rudderstack/rudder-sdk-node");
const jsonSize = require("json-size");
const csv = require("csvtojson/v2");
const dataToMigrate = require("./audience.json").data;

// async function convertToJson () {
//   let data
//   await csv()
//     .fromFile("./data.csv")
//     .then(jsonObj => {
//       // console.log(jsonObj);
//       data = jsonObj
//     });
//   return data
// }

async function sendIdentify(obj, identity) {
  // const endpoint = "http://localhost:4000"; //change to your backend endpoint
  // const projectId = "11111111-1111-1111-1111-111111111111"; //change to your projectId
  // const sourceId = "96a1d094-6818-4049-98e0-49855ebc874a"; //change to your sourceId
  const endpoint = "https://bri-backend.digitallab.id"; //change to your backend endpoint
  const projectId = "f20c758f-a2fd-4642-b152-14c70d4b7cdb"; //change to your projectId
  const sourceId = "907e6509-8fd6-419e-bf07-af3ccbb8d06c"; //change to your sourceId
  const AXP_BACKEND_HOST = `${endpoint}/${projectId}/${sourceId}/v1/batch`;
  const client = new aixpTracker(
    // "1ldtoh5I4vEyaBnsn025wYoW32H",
    "1lgCItJ0FN5qSusbvZVESHjEHLq", //change to your writekey
    AXP_BACKEND_HOST
  );
  const identify = await client.identify({
    userId: identity,
    anonymousId: faker.random.uuid(),
    traits: { ...obj, regisId: identity }, //change identity according to data manager: email, noTelp etc
  });
  return identify;
}

async function migrateData() {
  // const dataToMigrate = await convertToJson()
  console.log(dataToMigrate, "data");
  const batchSize = 100;
  let batches = [];
  for (var i = 0; i < dataToMigrate.length; i += batchSize) {
    batches = dataToMigrate.slice(i, i + batchSize);
    for (let j = 0; j < batches.length; j++) {
      const identity = "regid"; //change identity according to data.csv
      let dataSize = {};
      for (let obj in batches[j]) {
        if (jsonSize({ ...dataSize, ...batches[j][obj] }) > 2650) {
          const identify = await sendIdentify(
            { ...dataSize },
            batches[j][identity]
          );
          console.log(identify, "response");
          console.log("send", jsonSize(dataSize), "size", obj);
          dataSize = {};
          dataSize[obj] = batches[j][obj];
          console.log(dataSize, "<<data", obj);
        } else {
          dataSize[obj] = batches[j][obj];
          console.log(jsonSize(dataSize), "<<<<", obj);
        }
      }
      if (JSON.stringify(dataSize) !== JSON.stringify({})) {
        const identify = await sendIdentify(
          { ...dataSize },
          batches[j][identity]
        );
        console.log(dataSize, "send last part");
      }
    }
    await new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("rep");
        batches = [];
        resolve("");
      }, 3000);
    });
    console.log(`batch ${i}`);
  }
}
console.log(migrateData());
