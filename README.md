# DataMigration

## Requirements:
1. Nodejs
2. npm or yarn

## Steps to migrate:
1. git clone https://gitlab.com/kevint4n/dataMigration.git
2. cd /dataMigration
3. copy your data to data.csv file or replace the data.csv file with your file.csv and rename it to data.csv
4. npm install or yarn install
5. node migrateData.js