const faker = require("faker");

const aixpTracker = require("@rudderstack/rudder-sdk-node");
const jsonSize = require("json-size");
const csv = require("csvtojson/v2");
const data = require("./event.json").data

async function migrateData() {
  // const endpoint = "http://localhost:4000"; //change to your backend endpoint
  // const projectId = "11111111-1111-1111-1111-111111111111"; //change to your projectId
  // const sourceId = "96a1d094-6818-4049-98e0-49855ebc874a"; //change to your sourceId
  const endpoint = "https://bri-backend.digitallab.id"; //change to your backend endpoint
  const projectId = "f20c758f-a2fd-4642-b152-14c70d4b7cdb"; //change to your projectId
  const sourceId = "907e6509-8fd6-419e-bf07-af3ccbb8d06c"; //change to your sourceId
  const AXP_BACKEND_HOST = `${endpoint}/${projectId}/${sourceId}/v1/batch`;
  const client = new aixpTracker(
    // "1ldtoh5I4vEyaBnsn025wYoW32H",
    "1lgCItJ0FN5qSusbvZVESHjEHLq", //change to your writekey
    AXP_BACKEND_HOST
  );
  for (var i = 0; i < data.length; i++) {
    const anonymousId = faker.random.uuid()
    // client.identify({
    //   userId: data[i].registrationId,
    //   anonymousId,
    //   traits: { regisId: data[i].registrationId } //change identity according to data manager: email, noTelp etc
    // });
    const changes = JSON.parse(data[i].status_changes)
    for (var j = 0; j < changes.length; j++) {
      console.log(changes[j])
      client.track({
        userId: data[i].registrationId,
        anonymousId,
        event: "perubahanStatus",
        properties: {
          regisId: data[i].registrationId,
          pinjamanId: data[i].loanId,
          status: `from ${changes[j].from} to ${changes[j].to}`
        },
        context: {
          traits: {
            regisId: data[i].registrationId,
            pinjamanId: data[i].loanId,
            status: `from ${changes[j].from} to ${changes[j].to}`
          }
        }
      })
    }
    await new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve("");
      }, 3000);
    });
  }
}
console.log(migrateData());
